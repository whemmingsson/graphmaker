class Prim {
  private Node m_visitedNode;
  private ArrayList<Node> m_visited;
  
  public Prim() {
    m_visited = new ArrayList<Node>();
  }
  
  public ArrayList<Edge> generateMST(Graph g) {    
    ArrayList<Edge> mst = new ArrayList<Edge>();
    
    if(g.getEdges().size() == 0) {
      return mst;
    }
    
    ArrayList<Node> allNodes = g.getNodes();
    m_visited.add(allNodes.get(0)); // Or any arbitrary starting node
    while(m_visited.size() < allNodes.size()) {
      Edge e = getShortestEdge(g);
      m_visited.add(m_visitedNode);
      mst.add(e);
    }
    
    return mst;
  }
  
  private Edge getShortestEdge(Graph g) {   
      float shortestDistance = Float.MAX_VALUE;
      Edge shortestEdge = null;
      for(Node v : m_visited) {
       for(Edge e : g.getEdges(v)) {
         float d = e.getLength(); // Or other cost function
         Node other = getOther(e, v);
         if(d < shortestDistance && !alreadyVisited(other)) {
            shortestDistance = d;
            shortestEdge = e;
            m_visitedNode = other;
          }
       }             
    }
    return shortestEdge;      
  }
  
  private Node getOther(Edge e, Node n) {
    if(e.getFromNode().getId() == n.getId()) {
      return e.getToNode();
    }
    return e.getFromNode();
  }
  
  private boolean alreadyVisited(Node n) {
    for(Node v : m_visited) {
      if(v.getId() == n.getId()) {
        return true;
      }
    }   
    return false;
  }  
}