static class MathHelper {  
  static boolean pointIsOnEdge(float x, float y, Edge e, int nodeRadius) {
      PVector from = e.getFromNode().getPosition();
      PVector to = e.getToNode().getPosition();    
      return pointIsOnEdge(from.x, from.y, to.x, to.y, x, y, nodeRadius); 
  }
  
  private static boolean pointIsOnEdge(float x1, float y1, float x2, float y2, float x0, float y0, int nodeRadius) {
    return distanceToLineMidpoint(x1, y1, x2, y2, x0, y0) < dist(x1,y1,x2,y2)/2 - nodeRadius &&
           distanceToLine(x1, y1, x2, y2, x0, y0) < 4;
  }
    
  private static float distanceToLineMidpoint(float x1, float y1, float x2, float y2, float x0, float y0) {
     PVector mouse = new PVector(x0, y0);
     PVector middlePoint = new PVector((x1 + x2)/2, (y1 + y2)/2);
     return mouse.dist(middlePoint);
  }
  
  private static float distanceToLine(float x1, float y1, float x2, float y2, float x0, float y0){
    float numerator = abs((y2 - y1)*x0 - (x2 - x1)*y0 + x2*y1 - y2*x1);   
    float denominator = sqrt(pow(y2-y1,2)+pow(x2-x1,2)); 
    return (float)numerator / denominator;
  }  
}

public class KeyHelper {
  final char COST_KEY_LOWER = 'c';
  final char COST_KEY_UPPER = 'C';
  final char GRID_KEY_LOWER = 'g';
  final char GRID_KEY_UPPER = 'G';
  final char PRINT_KEY_LOWER = 'p';
  final char PRINT_KEY_UPPER = 'P';
  final char OPEN_KEY_LOWER = 'o';
  final char OPEN_KEY_UPPER = 'O';
  final char RUN_KEY_LOWER = 'r';
  final char RUN_KEY_UPPER = 'R';

  public boolean keyIsCostKey() {
    return key == COST_KEY_LOWER || key == COST_KEY_UPPER;
  }
  
  public boolean keyIsGridKey() {
    return key == GRID_KEY_LOWER || key == GRID_KEY_UPPER;
  }
  
  public boolean keyIsPrintKey() {
    return key == PRINT_KEY_LOWER || key == PRINT_KEY_UPPER;
  }

  public boolean keyIsOpenKey() {
    return key == OPEN_KEY_LOWER || key == OPEN_KEY_UPPER;
  }
  
  public boolean keyIsRunKey() {
    return key == RUN_KEY_LOWER || key == RUN_KEY_UPPER;
  } 
}  