interface IDrawMode {
  String getText();
  String getHelpText();
  void drawAction();
}

class NodeMode implements IDrawMode {
  String getText() {
    return "NODES";
  }
  
  String getHelpText() {
    return "Click anywhere to add a node. Hover a node to see connected edges. Click a node to delete it (and any connected edges).";
  }
       
  void drawAction() {
    Node currentMouseNode = getNodeUnderMouse();
    if(currentMouseNode != null){
      currentMouseNode.setMouseOver(true); 
      cursor(HAND);
      for(Edge e : graph.getEdges(currentMouseNode)){      
         e.setSelected(true);
       }
    }
    else {
       for(Node n : graph.getNodes()){ 
         if(n.getMouseOver()) {
           n.setMouseOver(false);
           cursor(ARROW);
           for(Edge e : graph.getEdges(n)){      
             e.setSelected(false);
           }
         }
       }        
    }
  }
}

class EdgeMode implements IDrawMode {
  String getText() {
    return "EDGES";
  }  
  
  String getHelpText() {
    return "Click any two nodes to create edge. Hold 'c' while clicking the second one to add an edge cost. Hover an edge to see cost/length. Click any edge to delete it.";
  }
    
  void drawAction() {
    Node currentMouseNode = getNodeUnderMouse();
    if(currentMouseNode != null){
      cursor(HAND);
      currentMouseNode.setMouseOver(true);   
    }
    else {
       for(Node n : graph.getNodes()){ 
         if(n.getMouseOver()) {
           cursor(ARROW);
           n.setMouseOver(false);
         }
       }        
    }
 
    for(Edge e : graph.getEdges()){       
        if(MathHelper.pointIsOnEdge(mouseX, mouseY, e, NODE_RADIUS)) {
           cursor(HAND);
           e.setMouseOver(true);
        }
        else if(e.getMouseOver()) {
           e.setMouseOver(false);
           cursor(ARROW);
        }      
     }
  }
}

class PrimMode implements IDrawMode {
  String getText() {
    return "Prim's algorithm";
  } 
  
  String getHelpText() {
    return "Runs Prim's algorithm. Press 'r' to run.";
  }
    
  void drawAction() {   
  }
}