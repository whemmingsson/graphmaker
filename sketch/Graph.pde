/* ------------------------------------------------------------- */
/* ---------------------- GRAPH CLASS -------------------------- */
/* ------------------------------------------------------------- */

class Graph {  
  private ArrayList<Node> m_nodes;
  private ArrayList<Edge> m_edges;
  
  private float m_totalCost;
  private float m_totalEdgeLength;
  
  Graph() {
    m_nodes = new ArrayList<Node>();
    m_edges = new ArrayList<Edge>();
    m_totalCost = 0;
    m_totalEdgeLength = 0;
  }
  
  void addNode(Node n) {
    n.setId(m_nodes.size());
    m_nodes.add(n);
  }
  
  void addNodeWithId(Node n) {
    m_nodes.add(n);
  }
  
  Node getNodeById(int id) {
     for(Node n : m_nodes) {  
       if(n.getId() == id) {
         return n;
       }
     }     
     return null;
  }
  
  void removeNode(Node n) {  
    ArrayList<Edge> toRemove = getEdges(n);
    m_nodes.remove(n);
       
    for(Edge e : toRemove) {
      removeEdge(e);
    }
  }
  
  void removeEdge(Edge e) {
      m_totalCost -= e.getCost();
      m_totalEdgeLength -= e.getLength();
      m_edges.remove(e);
  }
  
  float getTotalEdgeLength() {
    return m_totalEdgeLength;
  }
  
  float getTotalCost() {
    return m_totalCost;
  }
  
  ArrayList<Edge> getEdges(Node node) {
    ArrayList<Edge> edges = new ArrayList<Edge>();   
    for(Edge e : m_edges) {
      if(e.getFromNode() == node || e.getToNode() == node){
        edges.add(e);
      }
    }    
    return edges;    
  }
  
  ArrayList<Edge> getEdges() {
    return m_edges;
  }
  
   ArrayList<Node> getNodes() {
    return m_nodes;
  }
      
  void addEdge(Node from, Node to) {
    Edge e = new Edge(from, to);  
    addEdge(e);
  }
   
   void addEdge(Edge e) {     
      if(edgeAlreadyExists(e)) {
        return;
      }
      m_edges.add(e);
      m_totalCost += e.getCost();
      m_totalEdgeLength += e.getLength();
   }
    
  private void renderEdges() {
   for(Edge e : m_edges) {  
      e.render();
   }
   for(Edge e : m_edges) {
     if(e.isPartOfMST()) {
       e.renderMST();
     }
    }
  }
  
  public Edge getEdge(Node f, Node t) {
    for(Edge e: m_edges) {
      if(e.getFromNode().getId() == f.getId() && e.getToNode().getId() == t.getId()) {
        return e;
      }
    }
    
    return null;
  }
  
  private void renderNodes() {
   for(Node n : m_nodes) {  
      n.render();
   }
  }
  
  private void renderTextOverlay() {
    for(Edge e : m_edges) {  
      if(e.getMouseOver()) {
        e.renderEdgeInfo();
        break;
      }
    } 
  }
  
  private boolean edgeAlreadyExists(Edge edge) {
    for(Edge e: m_edges) {
      if(e.getFromNode() == edge.getFromNode() && 
         e.getToNode() == edge.getToNode()){
           return true;
         }
     }
     
     return false;
  }
    
  void render() {
    renderEdges();
    renderNodes();
    renderTextOverlay();
  }
  
  void cleanUp() {
    // Clean up any selected node
    for(Node n : m_nodes) {  
      n.setSelected(false);
      n.setMouseOver(false);
    }
      
    // Clean up any selected edges
    for(Edge e : m_edges) {  
      e.setSelected(false);
      e.setMouseOver(false);
    }
     
     // Clean up some global variables
     clickedNode = null;
  }
  
  Node getNodeFromPosition(float x, float y) {
    for(Node n : m_nodes) {
        if(n.distanceTo(x,y) < NODE_RADIUS) {
          return n;
        }
     }
     
     return null;
  }
  
  Edge getEdgeFromPosition(float x, float y) {
    for(Edge e : m_edges) {
        if(MathHelper.pointIsOnEdge(x, y, e, NODE_RADIUS)) {
          return e;
        }
     }
     
     return null;
  }
  
  void printToJsonFile() {
    JsonWriter writer = new JsonWriter(this, "g.json");
    writer.write();
  }
  
   void printToJsonFile(String path) {
    JsonWriter writer = new JsonWriter(this, path);
    writer.write();
  }
}

/* ------------------------------------------------------------- */
/* ----------------------- NODE CLASS -------------------------- */
/* ------------------------------------------------------------- */

public class Node {
  private PVector m_position;
  private String m_label;
  private int m_id;
   
  private boolean m_mouseOver;
  private boolean m_isSelected; // Is clicked by the user
    
  Node(PVector position) {
    m_position = position;
  }
  
  Node(float x, float y) {
    m_position = new PVector(x, y);
  }
  
  Node(float x, float y, int id) {
    m_id = id;
    m_position = new PVector(x, y);
  }
  
  void setId(int id) {
    m_id = id;
  }
  
  int getId() {
    return m_id;
  }
  
  PVector getPosition() {
    return m_position;
  }
  
  float distanceTo(Node other) {
    PVector otherPosition = other.getPosition();
    return dist(m_position.x, m_position.y,otherPosition.x, otherPosition.y);
  }
  
  float distanceTo(float x, float y) {
    return dist(m_position.x, m_position.y,x, y);
  }
  
  void setMouseOver(boolean over){
    m_mouseOver = over;
  }
  
   void setSelected(boolean selected){
    m_isSelected = selected;
  }
  
   boolean getMouseOver() {
    return m_mouseOver;
  }
    
  void render() {
   noStroke();
   fill(theme.nodeColor());
   
   if(m_mouseOver && mode instanceof EdgeMode) {
     fill(theme.nodeMouseOverColor());
   }
   else if(m_mouseOver && mode instanceof NodeMode) {
      fill(theme.nodeToDeleteColor());
   }
   else if (m_isSelected) {
     fill(theme.nodeSelectedColor());
   }
   
   ellipse(m_position.x, m_position.y, NODE_RADIUS * 2, NODE_RADIUS * 2);
  }        
}

/* ------------------------------------------------------------- */
/* ----------------------- EDGE CLASS -------------------------- */
/* ------------------------------------------------------------- */

public class Edge {
  private Node m_from;
  private Node m_to;
  private int m_cost; // Use a specific cost instead of the distance. Sometimes refered to as 'weight'.  
  private boolean m_mouseOver;
  private boolean m_isSelected; // Selected for deletion
  
  private boolean m_isPartOfMST; // Temporary variable for testing
  
  Edge(Node from, Node to) {
    m_from = from;
    m_to = to;
  }
  
   Edge(Node from, Node to, int cost) {
    m_from = from;
    m_to = to;  
    m_cost = cost;
  }  
      
  float getLength() {
   return m_from.distanceTo(m_to);
  }
  
  float getCost() {
    return m_cost;
  }
  
  void setCost(int cost) {
    m_cost = cost;
  }
  
  Node getFromNode() {
    return m_from;
  }
  
   Node getToNode() {
    return m_to;
  }
  
  void setMouseOver(boolean over){
    m_mouseOver = over;
  }
  
  boolean getMouseOver() {
    return m_mouseOver;
  }
  
  void setSelected(boolean selected){
    m_isSelected = selected;
  }
  
  void setMSTStatus(boolean isPartOfMST) {
    m_isPartOfMST = isPartOfMST;
  }
  
  public boolean isPartOfMST() {
    return m_isPartOfMST;
  }
  
  void render() { 
    fill(theme.edgeColor());
    stroke(theme.edgeColor());
    strokeWeight(2);
    
    if(m_isSelected) {
      fill(theme.edgeToDeleteColor());
      stroke(theme.edgeToDeleteColor());
    }
    else if(m_mouseOver) {
      fill(theme.edgeMouseOverColor());
      stroke(theme.edgeMouseOverColor());      
    }
      
    PVector f = m_from.getPosition();
    PVector t = m_to.getPosition();
    line(f.x, f.y, t.x,t.y);
    arrow(f.x, f.y, t.x,t.y);   
  }
  
  void renderMST() {
      strokeWeight(4);
      fill(theme.mstEdgeColor());
      stroke(theme.mstEdgeColor());   
      PVector f = m_from.getPosition();
      PVector t = m_to.getPosition();
      line(f.x, f.y, t.x,t.y);
  }
  
  void renderEdgeInfo() {
    noStroke();
    fill(0,0,0, 100);
    rect(mouseX+15, mouseY-10, 75, 50);
    fill(255);
    text("L: " + floor(getLength()), mouseX+20, mouseY+10);
    text("C: " + floor(getCost()), mouseX+20, mouseY+30);
  }
  
  void arrow(float x1, float y1, float x2, float y2) {
      pushMatrix();
      translate(x2, y2);
      float a = atan2(x1-x2, y2-y1);
      rotate(a);     
      triangle(0, -NODE_RADIUS - 2, 5, -8 - NODE_RADIUS -1, -5, -8 -NODE_RADIUS -1);
      popMatrix();
  } 
}