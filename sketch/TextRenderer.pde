class TextRenderer {  
  // Strings
  private String lengthStr = "Total edge length: %1$.0f";
  private String costStr = "Total cost: %1$.0f";
  private String printStr = "Press 'p' to save the graph to a json file";
  private String openStr = "Press 'o' to open a graph previously saved to file";

  void renderText() {
    fill(theme.textColor());
    textSize(16);
    text("Current mode: " + mode.getText(), 170, 20);
    
    textSize(13);   
    text(mode.getHelpText(), 170, 40);    
    text(printStr, 170, 60);
    text(openStr, 170, 80);
    
    textSize(16);    
    text(String.format(lengthStr, graph.getTotalEdgeLength()), width - 250, height - 20);
    text(String.format(costStr, graph.getTotalCost()), width - 250, height - 45);
  }
}  