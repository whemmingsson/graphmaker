class Grid {  
  private boolean m_renderGrid = true;
  private int m_cellSize = 80; // width/height for each grid cell in pixels
    
  void toggle() {
     m_renderGrid = !m_renderGrid;
  }
    
  void render() {
    if(!m_renderGrid) {
      return;
    }
    
    strokeWeight(1);
    stroke(40);
    for(int c = menu.getWidth(); c < width; c+=m_cellSize) {
      line(c, 0, c, height);
    }
    
    for(int c = 0; c < height; c+=m_cellSize) {
       line(menu.getWidth(), c, width, c);
    }
  }
}