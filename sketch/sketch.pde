// Constants
final int NODE_RADIUS = 10;
final int ANIMATION_INTERVAL = 250;

// Global instances
Edge workingEdge;
Console costConsole;
Console pathConsole;
Console pathReadConsole;
IDrawMode mode;
Graph graph;
Node clickedNode = null;
boolean mouseOverMenu;
ColorTheme theme;
EventHandler eventHandler;
KeyHelper keyHelper;
TextRenderer textRenderer;
Grid grid;
Menu menu;

// MST
ArrayList<Edge> m_MST;
ArrayList<Edge> m_MSTComplete;
int timer;

void setup(){
  // Sketch setup
  size(1200, 800);
  background(30);
  textSize(16);
  smooth();
  
  // Variable setup
  m_MSTComplete = new ArrayList<Edge>();
  mode = new NodeMode();
  graph = new Graph();
  theme = new ColorTheme();
  costConsole = new Console(new EdgeCostBehavior());
  pathConsole = new Console(new PathBehavior());
  pathReadConsole = new Console(new PathBehavior());
  eventHandler = new EventHandler();
  keyHelper = new KeyHelper();
  textRenderer = new TextRenderer();
  grid = new Grid();
  menu = new Menu();
  addMenuItems();    
}

void draw(){
  background(30);
  mode.drawAction();    
  grid.render();
  menu.render();
  textRenderer.renderText();  
  graph.render(); 
  costConsole.render();    
  pathConsole.render();
  pathReadConsole.render();
    
  handleMenu();
    
  // MST ANIMATED RENDERING -- how to improve? 
  if(m_MST != null) {
    if (millis() - timer >= ANIMATION_INTERVAL && m_MST.size() > 0) {
      timer = millis();
      m_MSTComplete.add(m_MST.get(0));
      m_MST.remove(0);
    }
    fill(0, 150);
    rect(menu.getWidth(),0, width - menu.getWidth(), height);
    for(Edge e: m_MSTComplete) {
      e.renderMST();
    }
  }  
}

void mousePressed() {
  eventHandler.mouse_Pressed();
}

void keyPressed() {  
  eventHandler.key_Pressed();
}

void addMenuItems() {
  menu.addMenuItem("Node mode", true, new NodeMode());
  menu.addMenuItem("Edge mode", false, new EdgeMode());
  menu.addMenuItem("Run Prim", false, new PrimMode());
}

void handleMenu() {
  if(mouseX < menu.getWidth() + NODE_RADIUS) {
    mouseOverMenu = true;
  }
  else {
   mouseOverMenu = false;
  }
  
  menu.unHoverAll();
  MenuItem item = menu.getMenuItemFromPosition(mouseX, mouseY);
  if(item != null) {
    item.setMouseOver(true);
  }
  else {
  }
}