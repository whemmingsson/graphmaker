class Menu {
  private final int ITEM_HEIGHT = 50;
  private final int ITEM_SPACING = 5;
  private final int TOP_OFFSET = 60;
  private PVector m_position;
  private int m_width;
  private ArrayList<MenuItem> m_menuItems;
  
  public Menu(int x, int y, int w) {
    m_position = new PVector(x, y);
    m_width = w; 
    m_menuItems = new ArrayList<MenuItem>();
  }
  
  public Menu() {
   this(0, 0, 160);   
  }
  
  public void addMenuItem(String text) {
    addMenuItem(text, false, null);
  }
  
  public void addMenuItem(String text, boolean selected, IDrawMode mode) {
    float y = calculateItemY();
    MenuItem item = new MenuItem(this, text, ITEM_HEIGHT, new PVector(m_position.x,y), mode);
    item.setSelected(selected);
    m_menuItems.add(item);
  }
  
  private float calculateItemY() {
    int s = m_menuItems.size();
    return (s * ITEM_HEIGHT) + TOP_OFFSET + (ITEM_SPACING*s);
  }
  
  public int getWidth() {
    return m_width;
  }
  
  public void render() {
   renderBackdrop();
   renderItems();
  }
  
  public MenuItem getMenuItemFromPosition(int mX, int mY) {
    for(MenuItem i : m_menuItems) {
      PVector p = i.getPosition();
      if(mX > p.x && mY > p.y && mX < p.x + m_width && mY < p.y + i.getHeight()) {
        return i;
      }
    }    
    return null;
  }
  
  public void unHoverAll() {
     for(MenuItem i : m_menuItems) {
       i.setMouseOver(false);
     }     
  }
  
  public void unSelectAll() {
    for(MenuItem i : m_menuItems) {
       i.setSelected(false);
     }
  }
    
  private void renderBackdrop() {
    fill(40, 180);
    rect(m_position.x, m_position.y, m_width, height - m_position.y);
    
    stroke(80);
    line(m_width, 0, m_width, height - m_position.y);
    
    textSize(36);
    fill(220);
    text("MENU", m_position.x + 30, m_position.y + 42);    
  }
  
  private void renderItems() {
    MenuItem hoverItem = null;
    for(int i = 0; i < m_menuItems.size(); i++) {
      MenuItem item = m_menuItems.get(i);      
      if(!item.getMouseOver()) {
        item.render(); 
      }
      else {
        hoverItem = item;
      }      
    }
    
    if(hoverItem != null) {
      hoverItem.render();
    }
  }
}

class MenuItem {  
  private String m_text;
  private int m_height;
  private PVector m_position;
  private Menu m_menu;
  private boolean m_mouseOver;
  private boolean m_isSelected;
  
  private IDrawMode m_mode;
  
  public MenuItem(Menu menu, String text, int h, PVector pos, IDrawMode mode) {  
    m_menu = menu;
    m_text = text;
    m_height = h;
    m_position = pos;
    m_mode = mode;
  }
  
  public int getHeight() {
    return m_height;
  }
  
  public PVector getPosition() {
    return m_position;
  }
  
  public boolean hasMode() {
    return m_mode != null;
  }
  
  public void render() {
    textAlign(BASELINE,CENTER);
    textSize(22);
    fill(100);
    
    if(m_mouseOver && m_isSelected) {
      stroke(250);
      fill(43, 170, 15);
      //cursor(HAND);
    }
    else if(m_mouseOver) {
      stroke(250);
      //cursor(HAND);
    }
    else if(m_isSelected) {
      fill(43, 170, 15);
      noStroke();    
    }
    else {
      noStroke();
      //cursor(ARROW);
    }
    
    rect(m_position.x+4, m_position.y, m_menu.getWidth()-8,m_height);
    fill(220);
    text(m_text, m_position.x + 10 + 2, m_position.y + m_height/2-2);
    textAlign(BASELINE);    
  }

  void setMouseOver(boolean over){
    m_mouseOver = over;
  }
  
   void setSelected(boolean selected){
    m_isSelected = selected;   
    if(hasMode() && selected) {
      updateMode();
    }
   }
   
   private void updateMode() {
     mode = m_mode; // Ugly as f*uck hack. How to refactor? Who should access global variables like this?
   }
  
   boolean getMouseOver() {
    return m_mouseOver;
  } 
}