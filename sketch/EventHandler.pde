class EventHandler {
  
  public void key_Pressed() {       
    // Handle the cost-console functionality
    if(costConsole.isRunning()){
      if(key == ENTER && workingEdge != null) {
        costConsole.stop();
        int value = costConsole.getValueAsInt();
        workingEdge.setCost(value);
        graph.addEdge(workingEdge);
      }
      else if (key == ESC) {
        pathConsole.stop();
      }
      else {
        costConsole.write(key);     
      }
      return;
    }
    
    // Handle the path-read-console functionality
    if(pathReadConsole.isRunning()){
      if(key == ENTER) {
        pathReadConsole.stop();
        String path = pathReadConsole.getValueAsString(); 
        JsonReader reader = new JsonReader(path);
        graph = reader.read();
      }
      else if (key == ESC) {
        pathReadConsole.stop();
      }
      else {
        pathReadConsole.write(key);     
      }
      return;
    }
    
    // Handle the path-console functionality
    if(pathConsole.isRunning()){
      if(key == ENTER) {
        pathConsole.stop();
        String path = pathConsole.getValueAsString(); 
        graph.printToJsonFile(path);
      }
      else {
        pathConsole.write(key);     
      }
      return;
    }   
    
    // Handle grid switching on/off
    if(keyHelper.keyIsGridKey()) {
      grid.toggle();
    }
    
    // Handle the print functionality
    if(keyHelper.keyIsPrintKey() && !pathConsole.isRunning() && !pathReadConsole.isRunning()) {
      pathConsole.start();
    }
    
     // Handle the open functionality
    if(keyHelper.keyIsOpenKey() && !pathConsole.isRunning() && !pathReadConsole.isRunning()) {
      pathReadConsole.start();
    }
    
    // Handle prim
    if(keyHelper.keyIsRunKey() && mode instanceof PrimMode) {
      Prim p = new Prim();
      m_MST = p.generateMST(graph);
    }
  }
   
  public void mouse_Pressed() {        
    if(anyConsoleRunning()) {
      return;
    }
    
    MenuItem item = menu.getMenuItemFromPosition(mouseX, mouseY);
    if(item != null) {
      menu.unSelectAll();
      item.setSelected(true);
      return;
    }
        
    Node n = getNodeUnderMouse();
    Edge e = getEdgeUnderMouse();
    if(mode instanceof NodeMode && !mouseOverMenu) {
      mousePressed_nodeMode(n); 
    }  
    else if(mode instanceof EdgeMode && !mouseOverMenu) {
      mousePressed_edgeMode(n,e); 
    }
  }
}

Node getNodeUnderMouse() {
  return graph.getNodeFromPosition(mouseX, mouseY);
}

Edge getEdgeUnderMouse() {
  return graph.getEdgeFromPosition(mouseX, mouseY);
}

private void mousePressed_nodeMode(Node n) {
  if(n == null) {
    graph.addNode(new Node(mouseX,mouseY));
  }
  else {
    cursor(ARROW);
    graph.removeNode(n);
  }
}

private void mousePressed_edgeMode(Node n, Edge e) {  
  if(e != null) {
    graph.removeEdge(e);
    cursor(ARROW);
  }
      
  if(n != null && clickedNode == null) {
     n.setSelected(true);
     clickedNode = n;
  }
  
  else if(n != null) {
    if(n != clickedNode) {
      if(keyPressed && keyHelper.keyIsCostKey()) {
        workingEdge = new Edge(clickedNode, n);
        costConsole.start();
      }
      else {
        graph.addEdge(clickedNode, n);
      }
    }
    clickedNode.setSelected(false);
    clickedNode = null;     
  }   
}

boolean anyConsoleRunning() {
  return costConsole.isRunning() || pathConsole.isRunning() || pathReadConsole.isRunning();
}