public class JsonWriter {  
  private Graph m_graph;
  private String m_path;
  
  public JsonWriter(Graph g, String path) {   
    m_graph = g;
    m_path = path;
  }
  
  public void write() {
    JSONObject json = new JSONObject();    
    JSONArray nodes = new JSONArray();
    JSONArray edges = new JSONArray();
    
    int c = 0;
    for(Node n : m_graph.getNodes()) {    
      nodes.setJSONObject(c,createJNode(n));
      c++;
    }
    
    c = 0;
     for(Edge e : m_graph.getEdges()) {    
      edges.setJSONObject(c,createJEdge(e));
      c++;
    }
        
    json.setJSONArray("nodes", nodes);
    json.setJSONArray("edges", edges);
    saveJSONObject(json, m_path);
  }
  
  private JSONObject createJNode(Node n) {
    JSONObject jNode = new JSONObject(); 
    jNode.setInt("id", n.getId());    
    jNode.setFloat("x", n.getPosition().x);
    jNode.setFloat("y", n.getPosition().y);         
    return jNode;
  }
  
  private JSONObject createJEdge(Edge e) {
    JSONObject jEdge = new JSONObject(); 
    jEdge.setInt("from", e.getFromNode().getId());    
    jEdge.setInt("to", e.getToNode().getId());
    jEdge.setFloat("cost", e.getCost());  
    return jEdge;
  }
}

class JsonReader {
  private String m_path;
  private Graph m_graph;
  
  public JsonReader(String path) {
    m_path = path;
  }
  
  public Graph read() { 
    JSONObject jGraph = loadJSONObject(m_path);
    JSONArray jNodes = jGraph.getJSONArray("nodes");
    JSONArray jEdges = jGraph.getJSONArray("edges");
    
    m_graph = new Graph();
    
    for(int i = 0; i < jNodes.size(); i++) {
      Node n = getNode(jNodes.getJSONObject(i));
      m_graph.addNodeWithId(n);
    }
    
    for(int i = 0; i < jEdges.size(); i++) {
      Edge e = getEdge(jEdges.getJSONObject(i));
      m_graph.addEdge(e);
    }
    
    return m_graph;            
  }
  
  Node getNode(JSONObject jNode) {
    return new Node(
      jNode.getFloat("x"), 
      jNode.getFloat("y"),
      jNode.getInt("id")
    );
  } 
  
  Edge getEdge(JSONObject jEdge) {
    Node from = m_graph.getNodeById(jEdge.getInt("from"));
    Node to = m_graph.getNodeById(jEdge.getInt("to"));
    int cost = jEdge.getInt("cost");
    println("FROM (id) = " + from.getId());
    println("TO (id) = " + to.getId());
    return new Edge(from, to, cost);
  }
}