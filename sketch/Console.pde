class Console{   
  private String m_value; 
  private PVector m_position;
  private int m_width;
  private int m_height; 
  private IConsoleBehavior m_behavior;  
  private boolean m_isActive;
     
  public Console(IConsoleBehavior behavior) {
    m_value =  "";
    m_width = 320;
    m_height = 50;
    m_position = new PVector(width / 2 - m_width/2, height / 2 - m_height/2);    
    m_behavior = behavior;
  }
  
  public void start() {
    m_value = "";
    m_isActive = true;
  }
  
  public void stop() {
    m_isActive = false;
    m_value = m_behavior.processOutput(m_value);
  }
  
  public String getValueAsString() {
    return m_value;
  }
  
  public int getValueAsInt() {
    if(m_value != "") {
      return Integer.valueOf(m_value);
    }
    return 0;
  }
  
  public float getValueAsFloat() {
    if(m_value != "") {
      return Float.valueOf(m_value);
    }
    return 0f;
  }
  
  public boolean isRunning() {
    return m_isActive;
  }
  
  public void write(char k) {
    m_value = m_behavior.processInput(k, m_value);
  }
        
  public void render() {
    if(!m_isActive) {
      return;
    }
    
    renderOverlay();
    renderConsole();
    renderInputLabel();
    renderInputValue();           
  }
  
  private void renderOverlay() {
    fill(0,0,0, 170);
    rect(0,0, width, height);
  }
  
  private void renderConsole() {
    fill(0,100,20);
    stroke(200);
    strokeWeight(2);
    rect(m_position.x, m_position.y, m_width, m_height);
  }
  
  private void renderInputLabel(){
    fill(255);
    text(m_behavior.getLabel(), m_position.x, m_position.y-5);
  }
  
  private void renderInputValue() {
    fill(255);
    textSize(20);
    text(">" + m_value, m_position.x + 10, m_position.y + 32);
  }  
}

interface IConsoleBehavior {
  String processInput(char k, String orignalValue);
  String processOutput(String orignalValue);
  String getLabel();
}

class EdgeCostBehavior implements IConsoleBehavior { 
  private final String LABEL = "Enter edge cost";
  private final int MAX_CHARACTERS = 8;
  private final char[] VALID_KEYS = new char[]{'1','2','3','4','5','6','7','8','9','0'};
    
  public String processInput(char k, String originalValue) {
    if(isValidKey(k) && originalValue.length() <= MAX_CHARACTERS) {
      return originalValue + k;
    }
    else if(k == BACKSPACE && originalValue.length() > 0){
      return originalValue.substring(0,originalValue.length()-1);
    }    
    return originalValue;
  }
  
  public String processOutput(String originalValue) {
    return originalValue; // We dont want to change the value in any way
  }
  
  public String getLabel() {
    return LABEL;
  }
     
  private boolean isValidKey(char k) {
    for(char c: VALID_KEYS) {
      if(k == c) {
        return true;
      }
    }    
    return false;
  }
}

class PathBehavior implements IConsoleBehavior {
   private final int MAX_CHARACTERS = 20;
   private final String LABEL = "Enter path to file";
   private final char[] VALID_EXTRA_KEYS = new char[]{'.', '/', '_', '-'};
  
   public String processInput(char k, String originalValue) {
    if((isAlphanumeric(k) || isValidExtraKey(k))  && originalValue.length() <= MAX_CHARACTERS) {
      return originalValue + k;
    }
    else if(k == BACKSPACE && originalValue.length() > 0){
      return originalValue.substring(0,originalValue.length()-1);
    }    
    return originalValue;
  }
  
  public String processOutput(String originalValue) {  
    if (originalValue.toLowerCase().contains(".json")) {
      return originalValue;
    }
    
    return originalValue + ".json";
  }
  
  public String getLabel() {
    return LABEL;
  }
  
  private boolean isValidExtraKey(char k) {
    for(char c: VALID_EXTRA_KEYS) {
      if(k == c) {
        return true;
      }
    }    
    return false;
  }
  
  private boolean isAlphanumeric(char c) {
    return !(c < 0x30 || (c >= 0x3a && c <= 0x40) || (c > 0x5a && c <= 0x60) || c > 0x7a);         
  }
}