class ColorTheme {
  public color nodeColor(){
    return color(70,140,250);
  }
  
  public color nodeMouseOverColor(){
    return color(255);
  }
  
  public color nodeSelectedColor(){
    return color(255, 222, 38);
  }
  
  public color nodeToDeleteColor(){
    return color(234, 76, 32);
  }
  
  public color edgeColor(){
    return color(50, 200, 80);
  }
  
   public color edgeToDeleteColor(){
    return color(234, 76, 32);
  }
  
  public color edgeMouseOverColor(){
    return color(234, 76, 32);
  }
  
   public color textColor(){
    return color(255);
  }
  
  public color mstEdgeColor() {
    return color(200);
  }
}